package kirill.collection;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.InputStream;
import java.net.URL;
import java.util.*;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;


public class TestInvertedIndex {
    private static Set<String> allKeys = new TreeSet<>();
    private static Set<String> allItems = new TreeSet<>();
    private static InvertedIndex<String, String> index;
    private static Map<String, List<String>> data = new TreeMap<String, List<String>>() {{
        put("1", asList("Существует три причины неявки : забыл , запил или забил".split(" ")));
        put("2", asList("".split(" ")));
        put("3", asList("Интернет как жизнь : делать нечего , а уходить не хочется !".split(" ")));
        put("4", asList("Есть два способа спорить с женщиной , ни один не помогает".split(" ")));
        put("5", asList("Ничто так не сбивает человека с мысли , как прямой удар в челюсть".split(" ")));
        put("6", asList("Жизнь коротка, потерпи немного...".split(" ")));
        put("7", asList("Господи , сколько ещё не сделано! А сколько ещё предстоит не сделать !".split(" ")));
        put("8", asList("Самое важное - уметь отличить важное от срочного".split(" ")));
        put("9", asList("Интернет , как и смерть , забирает лучших".split(" ")));
    }};

    @BeforeClass
    static public void initClass() {
        Iterator<Map.Entry<String, List<String>>> it = data.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, List<String>> pair = it.next();
            allItems.add(pair.getKey());
            allKeys.addAll(pair.getValue());
        }
    }

    @Before
    public void init() {
        index = new InvertedIndex<>();
        Iterator<Map.Entry<String, List<String>>> it = data.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, List<String>> pair = it.next();
            index.addItem(pair.getKey(), pair.getValue());
        }
    }

    List<String> itToList(Iterator<String> it) {
        List<String> res = new LinkedList<>();
        while (it.hasNext()) {
            res.add(it.next());
        }
        res.sort(String::compareTo);
        return res;
    }

    @Test
    public void testAddItem() {
        index.addItem("6","электрофикация");
        Iterator<String> it = index.findIntersection(data.get("6").iterator());
        String[] res = {"6"};
        assertEquals(itToList(it), asList(res));
    }

    @Test
    public void testFindIntersection6() {
        Iterator<String> it = index.findIntersection(data.get("6").iterator());
        String[] res = {"6"};
        assertEquals(itToList(it), asList(res));
    }

    @Test
    public void testFindIntersection1Key6Items() {
        String[] que = {","};
        Iterator<String> it = index.findIntersection(asList(que).iterator());
        String[] res = {"1", "3", "4", "5", "7", "9"};
        Arrays.sort(res);
        assertEquals(itToList(it), asList(res));
    }

    @Test
    public void testFindIntersection1Key1Item() {
        String[] que = {"удар"};
        Iterator<String> it = index.findIntersection(asList(que).iterator());
        String[] res = {"5"};
        Arrays.sort(res);
        assertEquals(itToList(it), asList(res));
    }

    @Test
    public void testFindIntersection1Key0Item() {
        String[] que = {"key"};
        Iterator<String> it = index.findIntersection(asList(que).iterator());
        String[] res = {};
        Arrays.sort(res);
        assertEquals(itToList(it), asList(res));
    }

    @Test
    public void testFindIntersection2Key0Item() {
        String[] que = {"key", "смерть"};
        Iterator<String> it = index.findIntersection(asList(que).iterator());
        String[] res = {};
        Arrays.sort(res);
        assertEquals(itToList(it), asList(res));
    }

    @Test
    public void testFindIntersection3Key0Item() {
        String[] que = {"ещё", "смерть", "и"};
        Iterator<String> it = index.findIntersection(asList(que).iterator());
        String[] res = {};
        Arrays.sort(res);
        assertEquals(itToList(it), asList(res));
    }

    @Test
    public void testFindIntersection2Key3Item() {
        String[] que = {":", ","};
        Iterator<String> it = index.findIntersection(asList(que).iterator());
        String[] res = {"1", "3"};
        Arrays.sort(res);
        assertEquals(itToList(it), asList(res));
    }

    @Test
    public void testFindUnion6() {
        Iterator<String> it = index.findUnion(data.get("6").iterator());
        String[] res = {"6"};
        assertEquals(itToList(it), asList(res));
    }

    @Test
    public void testFindUnion1Key6Items() {
        String[] que = {","};
        Iterator<String> it = index.findUnion(asList(que).iterator());
        String[] res = {"1", "3", "4", "5", "7", "9"};
        Arrays.sort(res);
        assertEquals(itToList(it), asList(res));
    }

    @Test
    public void testFindUnion1Key1Item() {
        String[] que = {"удар"};
        Iterator<String> it = index.findUnion(asList(que).iterator());
        String[] res = {"5"};
        Arrays.sort(res);
        assertEquals(itToList(it), asList(res));
    }

    @Test
    public void testFindUnion1Key0Item() {
        String[] que = {"key"};
        Iterator<String> it = index.findUnion(asList(que).iterator());
        String[] res = {};
        Arrays.sort(res);
        assertEquals(itToList(it), asList(res));
    }

    @Test
    public void testFindUnion2Key1Item() {
        String[] que = {"key", "смерть"};
        Iterator<String> it = index.findUnion(asList(que).iterator());
        String[] res = {"9"};
        Arrays.sort(res);
        assertEquals(itToList(it), asList(res));
    }

    @Test
    public void testFindUnion2Key0Item() {
        String[] que = {"ещё", "смерть"};
        Iterator<String> it = index.findUnion(asList(que).iterator());
        String[] res = {"9", "7"};
        Arrays.sort(res);
        assertEquals(itToList(it), asList(res));
    }

    @Test
    public void testFindUnion2Key3Item() {
        String[] que = {":", ","};
        Iterator<String> it = index.findUnion(asList(que).iterator());
        String[] res = {"1", "3", "4", "5", "7", "9"};
        Arrays.sort(res);
        assertEquals(itToList(it), asList(res));
    }
}
