package kirill;

public class WrongFactoryParameterException extends Exception {
    public WrongFactoryParameterException() {
        super();
    }
    public WrongFactoryParameterException(String param) {
        super("Wrong parameter: \"" + param + "\"");
    }
}
