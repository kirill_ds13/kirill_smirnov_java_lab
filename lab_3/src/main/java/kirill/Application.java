package kirill;

import kirill.collection.Index;
import kirill.collection.InvertedIndex;
import org.apache.commons.cli.*;

import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;


class FileAndUrlFactory implements StreamFactory {
    public InputStream createIn(String opt) throws IOException, WrongFactoryParameterException {
        String prefix;
        if (opt.startsWith(prefix = "url:")) {
            return new BufferedInputStream(new URL(opt.substring(prefix.length())).openStream());
        } else if (opt.startsWith(prefix = "file:")) {
            return new BufferedInputStream(new FileInputStream(opt.substring(prefix.length())));
        } else {
            throw new WrongFactoryParameterException(opt);
        }

    }
}


public class Application {

    static private String srcListFileName;
    static private Mode mode = Mode.HELP;
    static private StreamFactory sFactory = new FileAndUrlFactory();


    static private Options initCommandLineOptions() {
        return new Options()
                .addOption("h", "help", false, "Help")
                .addOption(Option.builder("f")
                        .desc("file containing a list of sources")
                        .hasArg()
                        .argName("name")
                        .longOpt("file")
                        .build());
    }

    static private void parseArgs(String[] args, Options options) throws ParseException {
        CommandLine commandLine = new DefaultParser().parse(options, args);
        if (commandLine.hasOption("f")) {
            mode = Mode.SEARCH;
            srcListFileName = commandLine.getOptionValue("f");
        }
    }

    private static Index<String, String> makeIndex(String fileName) throws IOException, WrongFactoryParameterException {
        Index<String, String> index = new InvertedIndex<>();
        try (BufferedReader srcListFile = new BufferedReader(new FileReader(fileName))) {
            String srcName;
            while ((srcName = srcListFile.readLine()) != null) {
                if (srcName.trim().compareTo("") != 0) {
                    try (Scanner src = new Scanner(sFactory.createIn(srcName)).useDelimiter("\\s+")) {
                        while (src.hasNext()) {
                            index.addItem(srcName, src.next());
                        }
                    }
                }
            }
        }
        return index;
    }
    static private void printIter(Iterator<String> it){
        while (it.hasNext()){
            System.out.println(it.next());
        }
    }

    public static void main(String[] args) {
        try {
            Options options = initCommandLineOptions();
            parseArgs(args, options);
            switch (mode) {
                case HELP:
                    HelpFormatter formatter = new HelpFormatter();
                    formatter.printHelp("Index", options, true);
                    break;
                case SEARCH:
                    Index<String, String> index = makeIndex(srcListFileName);
                    System.out.println("Index created");
                    System.out.println("input \"h\"");
                    System.out.print(">>>");
                    Scanner in = new Scanner(System.in);
                    while (in.hasNext()) {
                        List<String> words = Arrays.asList(in.nextLine().split("\\s+"));
                        Iterator<String> it = words.iterator();
                        Iterator<String> res;
                        if (it.hasNext())
                            switch (it.next()) {
                                case "i":
                                    res = index.findIntersection(it);
                                    printIter(res);
                                    break;
                                case "u":
                                    res = index.findUnion(it);
                                    printIter(res);
                                    break;
                                case "h":
                                    System.out.print(
                                            "\"i один два три\" - поиск файла который содержит все эти слова\n" +
                                            "\"u один два три\" - поиск файла который содержит хотя бы одно из этих слов\n" +
                                            "\"q\" - выход\n"
                                    );
                                    break;
                                case "q":
                                    System.exit(0);
                                    break;
                                default:
                                    //System.out.println("");
                            }
                        System.out.print(">>>");
                    }
                    break;
            }

        } catch (Exception exp) {
            System.err.println(exp.getMessage());
        }
    }

    private enum Mode {
        SEARCH,
        HELP
    }
}
