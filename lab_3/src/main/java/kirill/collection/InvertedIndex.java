package kirill.collection;

import com.google.common.collect.Sets;
import org.jetbrains.annotations.Contract;

import java.util.*;

public class InvertedIndex<Key, Item> implements Index<Key, Item> {
    private final Iterator<Item> emptyIterator = new Iterator<Item>() {
        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public Item next() {
            throw new NoSuchElementException();
        }
    };
    private Map<Key, Set<Item>> index = new HashMap<>();
    private Set<Item> allItem;

    public InvertedIndex() {
        this.allItem = createSet();
    }

    @Contract(" -> !null")
    private SortedSet<Item> createSet() {
        return new TreeSet<Item>();
    }

    @Override
    public void addItem(Item item, Key key) {
        Set<Item> set = index.get(key);
        if (set == null) {
            set = createSet();
            index.put(key, set);
        }
        set.add(item);
        allItem.add(item);
    }

    @Override
    public void addItem(Item item, Iterator<Key> keys) {
        while (keys.hasNext()) {
            Key key = keys.next();
            Set<Item> set = index.get(key);
            if (set == null) {
                set = createSet();
                index.put(key, set);
            }
            set.add(item);
        }
        allItem.add(item);
    }

    @Override
    public Iterator<Item> findIntersection(Iterator<Key> keys) {
        Set<Item> res;
        if (!keys.hasNext() || (res = index.get(keys.next())) == null) {
            return emptyIterator;
        }

        while (keys.hasNext()) {
            Set<Item> set = index.get(keys.next());
            if (set == null) {
                return emptyIterator;
            }
            res = Sets.intersection(res, set);
            if (res.size() == 0) {
                return emptyIterator;
            }
        }
        return res.iterator();
    }

    @Override
    public Iterator<Item> findUnion(Iterator<Key> keys) {
        Set<Item> res = null;
        for (; keys.hasNext() && res == null; res = index.get(keys.next())) ;
        if (res == null) {
            return emptyIterator;
        }
        while (keys.hasNext()) {
            Set<Item> set = index.get(keys.next());
            if (set != null) {
                res = Sets.union(res, set);
            }
        }
        return res.iterator();
    }

    @Override
    public Iterator<Key> keys() {
        return index.keySet().iterator();
    }

    @Override
    public Iterator<Item> values() {
        return allItem.iterator();
    }
}
