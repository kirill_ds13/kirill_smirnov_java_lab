package kirill.collection;

import java.util.Iterator;

/**
 * Created by kirill on 08.12.16.
 */
public interface Index<Key, Item> {
    void addItem(Item item, Iterator<Key> keys);
    void addItem(Item item, Key keys);
    Iterator<Item> findIntersection(Iterator<Key> keys);
    Iterator<Item> findUnion(Iterator<Key> keys);
    Iterator<Key> keys();
    Iterator<Item> values();
    default void addItem(Item item, Iterable<Key> keys){
        addItem(item, keys.iterator());
    }
}
