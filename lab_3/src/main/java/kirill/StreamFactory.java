package kirill;

import java.io.IOException;
import java.io.InputStream;

public interface StreamFactory {
    InputStream createIn(String opt) throws IOException, WrongFactoryParameterException;
}
