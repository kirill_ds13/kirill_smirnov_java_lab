package kirill.encryption;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

public class TestCipherInputStream {
    private RC4 kg;
    private byte[] key = {-11, 26, -89, 37, -54, -1, 82, 93, 51, 1, -44};
    @Before
    public void init() {
        kg = new RC4(key);
    }

    private void testReadByte(int n, int seed) throws IOException {
        byte[] buf = new byte[n];
        new Random(seed).nextBytes(buf);
        try (InputStream in = new CipherInputStream(new ByteArrayInputStream(buf), new RC4(kg))) {
            for (int i = 0; i < n; i++) {
                int a = in.read();
                int b = buf[i] & 0xFF;
                int c = kg.generateByte();
                int d = c ^ a;
                Assert.assertEquals(d, b);
            }
        }
    }

    private void testReadMas(int n, int m, int seed) throws IOException {
        byte[] buf1 = new byte[n * m];
        byte[] buf2 = new byte[m];
        new Random(seed).nextBytes(buf1);
        try (InputStream in = new CipherInputStream(new ByteArrayInputStream(buf1), new RC4(kg))) {
            for (int i = 0; i < n; i++) {
                in.read(buf2);
                for (int j = 0; j < m; j++) {
                    buf2[j] ^= kg.generateByte();
                    Assert.assertEquals(buf2[j], buf1[i * m + j]);
                }
            }
        }
    }

    @Test
    public void testReadStreamEnd() throws IOException {
        try (InputStream in = new CipherInputStream(new ByteArrayInputStream(new byte[0]), new RC4(kg))) {
            Assert.assertEquals(in.read(),-1);
        }
    }
    @Test
    public void testReadByte40_21() throws IOException {
        testReadByte(40, 21);
    }

    @Test
    public void testReadMas1_200_234() throws IOException {
        testReadMas(1, 200, 234);
    }

    @Test
    public void testReadMas40_10_33() throws IOException {
        testReadMas(40, 10, 33);
    }

    @Test
    public void testReadMas400_20_303() throws IOException {
        testReadMas(400, 20, 303);
    }

}
