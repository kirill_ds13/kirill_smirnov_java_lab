package kirill.encryption;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.Random;

public class TestCipherOutputStream {
    private KeyStreamGenerator kg;
    private byte[] key = {-11, 26, -89, 37, -54, -1, 82, 93, 51, 1, -44};
    private CipherOutputStream testOut;
    private ByteArrayOutputStream out;
    @Before
    public void init() {
        kg = new RC4(key);
        out = new ByteArrayOutputStream();
        testOut = new CipherOutputStream(out, new RC4(key));
    }

    private void testWriteByte(int n,int seed) throws IOException {
        byte[] buf = new byte[n];
        new Random(seed).nextBytes(buf);
        for(byte b:buf){
            testOut.write(b);
        }
        byte[] t = out.toByteArray();
        for (int i = 0; i < t.length; i++) {
            int g = kg.generateByte()&0xFF;
            t[i] ^= g;
        }
        Assert.assertArrayEquals(t, buf);                                                                                                                                                                                                                                                                                                   Assert.assertArrayEquals(t, buf);
    }
    private void testWriteMas(int n, int seed) throws IOException {
        testWriteMas(1, n, seed);
    }
    private void testWriteMas(int m, int n, int seed) throws IOException {
        byte[] buf = new byte[n*m];
        new Random(seed).nextBytes(buf);
        for (int i = 0; i < m; i++) {
            testOut.write(buf, i*n, n);
        }
        byte[] t = out.toByteArray();
        for (int i = 0; i < t.length; i++) {
            int g = kg.generateByte()&0xFF;
            t[i] ^= g;
        }
        Assert.assertArrayEquals(t, buf);                                                                                                                                                                                                                                                                                                   Assert.assertArrayEquals(t, buf);
    }

    @Test
    public void testWriteByteToBigNum() throws IOException{
        testOut.write(0xFFFF);
        byte[] buf = out.toByteArray();
        buf[0] ^= kg.generateByte()&0xFF;
        Assert.assertEquals(buf[0], (byte)0xFF);
    }
    @Test
    public void testWriteByteEmptyArr() throws IOException{
        testOut.write(new byte[0]);
        byte[] buf = out.toByteArray();
        Assert.assertEquals(buf.length, 0);
    }
    @Test
    public void testWriteByte40_21() throws IOException{
        testWriteByte(40,21);
    }
    @Test
    public void testWriteByte20_46() throws IOException{
        testWriteByte(20,46);
    }
    @Test
    public void testWriteMas40_10() throws IOException{
        testWriteMas(40,10);
    }
    @Test
    public void testWriteMas111_27() throws IOException{
        testWriteMas(111,27);
    }
    @Test
    public void testWriteMas47_40_10() throws IOException{
        testWriteMas(47, 40, 10);
    }
    @Test
    public void testWriteMas11_111_27() throws IOException{
        testWriteMas(11, 111,27);
    }
}
