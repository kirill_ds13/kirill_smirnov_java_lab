package kirill;

import kirill.encryption.CipherInputStream;
import kirill.encryption.CipherOutputStream;
import kirill.encryption.RC4;
import org.apache.commons.cli.*;

import java.io.*;

public class Application {


    static private final int BUFFER_SIZE = 1000;
    static private Mode mode;
    static private String password;
    static private String inputFileName;
    static private String outputFileName;

    static private Options initCommandLineOptions() {
        return new Options()
                .addOption("h", "help", false, "Help")
                .addOption(Option.builder("e")
                        .desc("encrypt input with password")
                        .hasArg()
                        .argName("password")
                        .longOpt("encrypt")
                        .build())
                .addOption(Option.builder("d")
                        .desc("decrypt input with password")
                        .hasArg()
                        .argName("password")
                        .longOpt("decrypt")
                        .build())
                .addOption(Option.builder("i")
                        .desc("input file")
                        .hasArg()
                        .argName("file")
                        .build())
                .addOption(Option.builder("o")
                        .desc("output file")
                        .hasArg()
                        .argName("file")
                        .build());
    }

    static private void parseArgs(String[] args, Options options) throws ParseException {
        CommandLine commandLine = new DefaultParser().parse(options, args);
        boolean isEncrypt = commandLine.hasOption("e");
        boolean isDecrypt = commandLine.hasOption("d");
        boolean isHelp = commandLine.hasOption("h");

        if ((!isEncrypt || isDecrypt || isHelp) &&
                (isEncrypt || !isDecrypt || isHelp) &&
                (isEncrypt || isDecrypt || !isHelp)) {
            throw new ParseException("wrong parameters\n for help use -h");
        } else if (isDecrypt) {
            mode = Mode.DECRYPTING;
            password = commandLine.getOptionValue("d");
        } else if (isEncrypt) {
            mode = Mode.ENCRYPTING;
            password = commandLine.getOptionValue("e");
        } else {
            mode = Mode.HELP;
        }

        if (commandLine.hasOption("i")) {
            inputFileName = commandLine.getOptionValue("i");
        }
        if (commandLine.hasOption("o")) {
            outputFileName = commandLine.getOptionValue("o");
        }

    }

    static private InputStream openInputStream(String fileName) throws FileNotFoundException {
        InputStream in = (fileName == null ? System.in : new FileInputStream(fileName));
        return (mode == Mode.DECRYPTING) ? (new CipherInputStream(in, new RC4(password.getBytes()))) : (in);
    }

    static private OutputStream openOutputStream(String fileName) throws FileNotFoundException {
        OutputStream out = (fileName == null ? System.out : new FileOutputStream(fileName));
        return (mode == Mode.ENCRYPTING) ? (new CipherOutputStream(out, new RC4(password.getBytes()))) : (out);
    }

    static private void fromInToOut(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        for (int i = in.read(buffer); i != -1; i = in.read(buffer)) {
            out.write(buffer, 0, i);
        }
    }

    public static void main(String[] args) {
        try {
            Options options = initCommandLineOptions();
            parseArgs(args, options);

            switch (mode) {
                case DECRYPTING:
                    try (InputStream in = openInputStream(inputFileName);
                         OutputStream out = openOutputStream(outputFileName)) {
                        fromInToOut(in, out);
                    }
                    break;
                case ENCRYPTING:
                    try (InputStream in = openInputStream(inputFileName);
                         OutputStream out = openOutputStream(outputFileName)) {
                        fromInToOut(in, out);
                    }
                    break;
                case HELP:
                    HelpFormatter formatter = new HelpFormatter();
                    formatter.printHelp("encryptor", options, true);
                    break;
            }
        } catch (Exception exp) {
            System.err.println(exp.getMessage());
        }
    }

    private enum Mode {
        ENCRYPTING,
        DECRYPTING,
        HELP
    }
}
