package kirill.encryption;

import org.jetbrains.annotations.NotNull;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class CipherOutputStream extends FilterOutputStream {
    private KeyStreamGenerator generator;

    public CipherOutputStream(@NotNull OutputStream out, @NotNull KeyStreamGenerator generator) {
        super(out);
        this.generator = generator;
    }

    @Override
    public void write(int b) throws IOException {
        int g = generator.generateByte() & 0xFF;
        super.write(b ^ g);
    }


}
