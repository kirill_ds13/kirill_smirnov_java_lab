package kirill.encryption;

public class RC4 implements KeyStreamGenerator {
    static private final int N = 8;
    static private final int SIZE_S = 2 << N; //2^n
    static private final int MAGIC_NUMBER = 768;

    private final short[] sBlock = new short[SIZE_S];
    private int i = 0, j = 0; // state

    public RC4(RC4 rc4) {
        System.arraycopy(rc4.sBlock, 0, sBlock, 0, rc4.sBlock.length);
        i = rc4.i;
        j = rc4.j;
    }

    public RC4(byte[] key) {
        for (int k = 0; k < SIZE_S; k++) {
            sBlock[k] = (short) k;
        }
        for (int k = 0; k < SIZE_S; k++) {
            j = (j + sBlock[i] + (key[i % key.length] & 0xFF)) % SIZE_S;
            //swap S[i] and S[j]
            short temp = sBlock[i];
            sBlock[i] = sBlock[j];
            sBlock[j] = temp;
        }

        //дополнительное перемешивание
        for (int k = 0; k < MAGIC_NUMBER; k++) {
            generateByte();
        }

    }

    public byte generateByte() {
        i = (i + 1) % SIZE_S;
        j = (j + sBlock[i]) % SIZE_S;
        //swap S[i] and S[j]
        short temp = sBlock[i];
        sBlock[i] = sBlock[j];
        sBlock[j] = temp;

        return (byte) sBlock[(sBlock[i] + sBlock[j]) % SIZE_S];
    }

    boolean check() {
        for (int k = 0; k < SIZE_S; k++) {
            if (sBlock[k] < 0 || sBlock[k] >= SIZE_S)
                return false;
        }
        if ((0 > i) || (0 > j) || (i >= SIZE_S) || (j >= SIZE_S)) {
            return false;
        }
        return true;
    }

}
