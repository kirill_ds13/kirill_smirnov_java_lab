package kirill.encryption;

import org.jetbrains.annotations.NotNull;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;


public class CipherInputStream extends FilterInputStream {
    private KeyStreamGenerator generator;

    public CipherInputStream(@NotNull InputStream in, @NotNull KeyStreamGenerator generator) {
        super(in);
        this.generator = generator;
    }

    @Override
    public int read() throws IOException {
        int ch = super.read();
        return (ch == -1) ? (-1) : (ch ^ generator.generateByte());
    }

    @Override
    public int read(@NotNull byte[] b) throws IOException {
        return read(b, 0, b.length);
    }

    @Override
    public int read(@NotNull byte[] b, int off, int len) throws IOException {
        int r = super.read(b, off, len);
        for (int i = off, end = off + r; i < end; i++) {
            int m = generator.generateByte();
            b[i] = (byte) (b[i] ^ m);
        }
        return r;
    }

    @Override
    public long skip(long n) throws IOException {
        assert (n >= 0);
        generator.skipNByte(n);
        return super.skip(n);
    }
}

