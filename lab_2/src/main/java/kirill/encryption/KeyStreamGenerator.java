package kirill.encryption;


public interface KeyStreamGenerator {
    byte generateByte();

    default int generateInt() {
        byte b0, b1, b2, b3;
        b0 = generateByte();
        b1 = generateByte();
        b2 = generateByte();
        b3 = generateByte();
        return (b0 & 0xFF << 24) & (b1 & 0xFF << 16) & (b2 & 0xFF << 8) & (b3 & 0xFF);
    }

    default void skipNByte(long n) {
        assert (n >= 0);
        for (long i = 0; i < n; i++) {
            generateByte();
        }
    }

    default void skipNInt(long n) {
        assert (n >= 0);
        for (long i = 0; i < n; i++) {
            generateInt();
        }
    }
}
