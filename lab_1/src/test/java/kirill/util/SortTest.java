package kirill.util;

import org.junit.Test;

import java.util.Arrays;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;


public class SortTest {

    private final Integer[] emptyArray = {};
    private final Integer[] oneElementArray = {6};
    private final Integer[][] twoElementArray = {
            {1, 6},
            {6, 1},
            {6, 6}
    };
    private final Integer[][] threeElementArray = {
            {1, 6, 7},
            {6, 1, 7},
            {6, 9, 6}
    };
    private final Integer[][] equalElementArray = {
            {6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6},
            {6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6},
    };
    private final Integer[][] sortedArray = {
            {1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
            {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11},
            {10, 9, 8, 7, 6, 5, 4, 3, 2, 1},
            {11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1}
    };
    private final Integer[][] almostSameElementsArray = {
            {0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0},
            {0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0},
            {0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1,
                    0, 1, 0, 1, 0, 1, 0, 1, 0},
            {1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0,
                    0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1,
                    0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1}
    };
    private final Integer[][] randomArray = {
            {4, 8, 2, 34, 3, 5, 42, 1, 9, 3, 0},
            {36, 46, 87, 9, 42, 58, 14, 45, 10, 73, 93, 1002, 12, 92, 718, 16, 2, 93, 359, 314, 9, 42, 39, 1, 82, 13,
                    87, 92, 17, 671, 16, 49, 5, 62, 8, 71, 90, 49, 86, 870, 100, 73, 39, 10, 864, 36, 3, 51},
            {30, 3, 76, 41, 80, 84, 60, 6, 1, 82, 5, 29, 86, 9, 8, 3, 59, 16, 60, 50, 5, 6, 87, 58, 63, 0, 30, 30, 14,
                    84, 84, 39, 41, 69, 33, 45, 54, 77, 23, 21, 90, 67, 99, 42, 96, 89, 36, 55, 30, 0, 7, 72, 68, 8, 3},
            {9, 45, 82, 69, 45, 8, 9, 84, 58, 794, 5, 8, 74, 5, 6, 8, 4, 9, 5, 8, 64, 76, 5, 64, 7, 8, 6, 7, 4, 5, 839, 8, 76, 8, 52, 3, 78, 67, 2},
            {5, 7, 4, 6, 7, 0, 2, 4, 7, 4, 6, 0, 9, 4, 9, 8, 2, 3, 6, 9}
    };

    private <T extends Comparable<? super T>> boolean isSorted(T[] m) {
        for (int i = 0; i < m.length - 1; i++) {
            if (m[i].compareTo(m[i + 1]) > 0) {
                return false;
            }
        }
        return true;
    }

    private <T> boolean isSame(T[] a, T[] b) {
        for (T x : a) {
            int s = 0;
            for (T y : b) {
                if (x == y) {
                    s++;
                }
            }
            for (T y : a) {
                if (x == y) {
                    s--;
                }
            }
            if (s != 0) {
                return false;
            }
        }
        return true;
    }

    private void testSort(Integer[] test, Sorter sorter) {
        Integer[] mas = Arrays.copyOf(test, test.length);

        sorter.run(mas);

        assertEquals(mas.length, test.length);
        assertTrue(isSame(test, mas));
        assertTrue(isSorted(mas));
    }

    //CombSort tests
    @Test
    public void testCombSortEmpty() {
        testSort(emptyArray, SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortOneElement() {
        testSort(oneElementArray, SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortTwoElement0() {
        testSort(twoElementArray[0], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortTwoElement1() {
        testSort(twoElementArray[1], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortTwoElement2() {
        testSort(twoElementArray[2], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortThreeElement0() {
        testSort(threeElementArray[0], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortThreeElement1() {
        testSort(threeElementArray[1], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortThreeElement2() {
        testSort(threeElementArray[2], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortEqualElement0() {
        testSort(equalElementArray[0], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortEqualElement1() {
        testSort(equalElementArray[1], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortSortedElement0() {
        testSort(sortedArray[0], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortSortedElement1() {
        testSort(sortedArray[1], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortSortedElement2() {
        testSort(sortedArray[2], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortSortedElement3() {
        testSort(sortedArray[3], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortAlmostSameElement0() {
        testSort(almostSameElementsArray[0], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortAlmostSameElement1() {
        testSort(almostSameElementsArray[1], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortAlmostSameElement2() {
        testSort(almostSameElementsArray[2], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortAlmostSameElement3() {
        testSort(almostSameElementsArray[3], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortRandom0() {
        testSort(randomArray[0], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortRandom1() {
        testSort(randomArray[1], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortRandom2() {
        testSort(randomArray[2], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortRandom3() {
        testSort(randomArray[3], SortAlgorithms::combSort);
    }

    @Test
    public void testCombSortRandom4() {
        testSort(randomArray[4], SortAlgorithms::combSort);
    }

    //SmoothSort tests
    @Test
    public void testSmoothSortEmpty() {
        testSort(emptyArray, SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortOneElement() {
        testSort(oneElementArray, SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortTwoElement0() {
        testSort(twoElementArray[0], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortTwoElement1() {
        testSort(twoElementArray[1], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortTwoElement2() {
        testSort(twoElementArray[2], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortThreeElement0() {
        testSort(threeElementArray[0], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortThreeElement1() {
        testSort(threeElementArray[1], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortThreeElement2() {
        testSort(threeElementArray[2], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortEqualElement0() {
        testSort(equalElementArray[0], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortEqualElement1() {
        testSort(equalElementArray[1], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortSortedElement0() {
        testSort(sortedArray[0], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortSortedElement1() {
        testSort(sortedArray[1], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortSortedElement2() {
        testSort(sortedArray[2], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortSortedElement3() {
        testSort(sortedArray[3], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortAlmostSameElement0() {
        testSort(almostSameElementsArray[0], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortAlmostSameElement1() {
        testSort(almostSameElementsArray[1], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortAlmostSameElement2() {
        testSort(almostSameElementsArray[2], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortAlmostSameElement3() {
        testSort(almostSameElementsArray[3], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortRandom0() {
        testSort(randomArray[0], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortRandom1() {
        testSort(randomArray[1], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortRandom2() {
        testSort(randomArray[2], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortRandom3() {
        testSort(randomArray[3], SortAlgorithms::smoothSort);
    }

    @Test
    public void testSmoothSortRandom4() {
        testSort(randomArray[4], SortAlgorithms::smoothSort);
    }

}

