package kirill.util;

import java.util.Comparator;


public interface Sorter {
    <T> void run(T[] mas, Comparator<? super T> comp);

    default <T extends Comparable<? super T>> void run(T[] mas) {
        run(mas, Comparable::compareTo);
    }
}
