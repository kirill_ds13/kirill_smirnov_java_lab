package kirill.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SortAlgorithms {

    private static final int[] leonardoNumber = {1, 1, 3, 5, 9, 15, 25, 41, 67, 109, 177, 287, 465, 753, 1219, 1973,
            3193, 5167, 8361, 13529, 21891, 35421, 57313, 92735, 150049, 242785, 392835, 635621, 1028457, 1664079,
            2692537, 4356617, 7049155, 11405773, 18454929, 29860703, 48315633, 78176337, 126491971, 204668309,
            331160281, 535828591, 866988873, 1402817465};

    @org.jetbrains.annotations.Contract(pure = true)
    private static int leoNum(int n) {
        assert (n >= 0 && n <= 43);
        return leonardoNumber[n];
    }

    private static <T> void swap(T[] mas, int i, int j) {
        T temp = mas[i];
        mas[i] = mas[j];
        mas[j] = temp;
    }

    private static <T> void heapify(T[] mas, int head, int lvl, Comparator<? super T> comp) {
        if (lvl >= 2) {
            int right = head - 1;
            int left = right - leoNum(lvl - 2);
            int largest = head;
            if (comp.compare(mas[left], mas[largest]) > 0) {
                largest = left;
            }
            if (comp.compare(mas[right], mas[largest]) > 0) {
                largest = right;
            }
            if (largest != head) {
                swap(mas, largest, head);
                if (largest == right) {
                    heapify(mas, largest, lvl - 2, comp);
                } else {
                    heapify(mas, largest, lvl - 1, comp);
                }
            }
        }
    }

    private static <T> void ensureSequence(T[] mas, List<Integer> heapLevels, Comparator<? super T> comp) {
        int curHeadPos = -1;
        for (Integer lvl : heapLevels) {
            curHeadPos += leoNum(lvl);
        }
        heapify(mas, curHeadPos, heapLevels.get(heapLevels.size() - 1), comp);
        for (int i = heapLevels.size() - 1; i > 0; i--) {
            int lvl = heapLevels.get(i);
            int nextHeadPos = curHeadPos - leoNum(lvl);
            if (comp.compare(mas[curHeadPos], mas[curHeadPos - leoNum(lvl)]) >= 0)
                break;

            swap(mas, nextHeadPos, curHeadPos);

            heapify(mas, nextHeadPos, heapLevels.get(i - 1), comp);

            curHeadPos = nextHeadPos;
        }
    }

    public static <T> void smoothSort(T[] mas, Comparator<? super T> comp) {
        if (mas.length < 2)
            return;
        List<Integer> heapLevels = new ArrayList<>();
        //
        for (int i = 0; i < mas.length; i++) {
            int n = heapLevels.size() - 1;
            if (heapLevels.size() >= 2 && (
                    heapLevels.get(n - 1) - heapLevels.get(n) == 1 ||
                            (heapLevels.get(n - 1) == 1 && heapLevels.get(n) == 1))
                    ) {
                heapLevels.remove(n);
                heapLevels.set(n - 1, heapLevels.get(n - 1) + 1);
            } else {
                heapLevels.add(1);
            }
            ensureSequence(mas, heapLevels, comp);
        }

        for (int i = mas.length - 1; i > 0; i--) {
            int n = heapLevels.size() - 1;
            int lvl = heapLevels.get(n);
            heapLevels.remove(n);
            if (lvl > 1) {
                heapLevels.add(lvl - 1);
                if (n > 0)
                    ensureSequence(mas, heapLevels, comp);
                heapLevels.add(lvl - 2);
                ensureSequence(mas, heapLevels, comp);
            }
        }
    }

    public static <T> void combSort(T[] a, Comparator<? super T> comp) {
        final double factor = 1.247;
        int step = a.length;
        boolean isSorted;
        do {
            isSorted = true;
            if (step > 1)
                step = (int) (step / factor);
            else
                step = 1;
            for (int i = 0; i < a.length - step; i++) {
                int c = comp.compare(a[i], a[i + step]);
                if (c > 0) {
                    isSorted = false;
                    swap(a, i, i + step);
                }
            }
        } while (!isSorted || step > 1);
    }
}
